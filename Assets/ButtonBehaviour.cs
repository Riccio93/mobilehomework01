using UnityEngine;
using UnityEngine.UI;

public class ButtonBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        gameObject.GetComponentInChildren<Text>().text = $"{gameObject.name} ON";
        gameObject.GetComponent<Image>().color = new Color(197f/255f, 253f/255f, 180f/255f);
    }
    
    public void ChangeButtonAppearance()
    {
        if(gameObject.GetComponentInChildren<Text>().text.Equals($"{gameObject.name} ON"))
        {
            gameObject.GetComponentInChildren<Text>().text = $"{gameObject.name} OFF";
            gameObject.GetComponent<Image>().color = new Color(248f / 255f, 142f / 255f, 142f / 255f);
        }
        else
        {
            gameObject.GetComponentInChildren<Text>().text = $"{gameObject.name} ON";
            gameObject.GetComponent<Image>().color = new Color(197f / 255f, 253f / 255f, 180f / 255f);
        }
    }
}
