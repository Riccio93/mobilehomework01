using UnityEngine;

public class SphereBehaviour : MonoBehaviour
{
    private static Vector3 min = new Vector3(-30f, -30f, -30f);
    private static Vector3 max = new Vector3(30f, 30f, 30f);

    [SerializeField] private float initialSpeed;
    [SerializeField] private float noMovementThreshold;
    private const int stillnessSecondsBeforeDeletingPlusOne = 4;
    private Vector3[] previousLocations = new Vector3[stillnessSecondsBeforeDeletingPlusOne];
    private bool isMoving;
    private bool noMovementImmunity;
    private float secsSinceLastMovementCheck = 0;

    private bool collisionImmunity;

    public GameObject mediumSphere;
    public GameObject smallSphere;

    private void SetInitialnoMovementImmunityOff()
    {
        noMovementImmunity = false;
    }

    private void SetInitialCollisionImmunityOff()
    {
        collisionImmunity = false;
    }

    public void Start()
    {
        //Assigned random direction in a cone
        transform.Rotate(Random.Range(min.x, max.x), Random.Range(min.y, max.y),Random.Range(min.z, max.z));
        GetComponent<Rigidbody>().AddForce(transform.forward * initialSpeed);

        //Initialization of the Vector3 that will contain the pos of the object in the previous x seconds
        previousLocations[previousLocations.Length - 1] = transform.position;
        for (int i = 0; i < previousLocations.Length - 2; i++)
        {
            previousLocations[i] = Vector3.zero;
        }

        //Cancels immunity from inactivity 3 secs after starting, and immunity from collision detection after 1 sec
        noMovementImmunity = true;
        Invoke("SetInitialnoMovementImmunityOff", 3f);
        collisionImmunity = true;
        Invoke("SetInitialCollisionImmunityOff", .5f);
    }

    public void FixedUpdate()
    {
        //if in the last x seconds the position of the object hasn't change a good amount (< noMovementTreshold) it's destroyed
        secsSinceLastMovementCheck += Time.fixedDeltaTime;
        if (secsSinceLastMovementCheck >= 1f)
        {
            for (int i = 0; i < previousLocations.Length - 1; i++)
            {
                previousLocations[i] = previousLocations[i + 1];
            }
            previousLocations[previousLocations.Length - 1] = transform.position;

            for (int i = 0; i < previousLocations.Length - 1; i++)
            {
                isMoving = false;
                if (Vector3.Distance(previousLocations[i], previousLocations[i + 1]) > noMovementThreshold)
                {
                    isMoving = true;
                    break;
                }
            }

            if (noMovementImmunity == false && isMoving == false)
            {
                Destroy(gameObject);
            }
            secsSinceLastMovementCheck = 0f;
        }
    }

    
    public void OnCollisionEnter(Collision collision)
    {
        if(collisionImmunity == false && collision.collider.gameObject.layer != 3) //layer 3 = floor
        {
            if (gameObject.name.Equals("LargeSphere(Clone)"))
            {
                Instantiate(mediumSphere, transform.position, transform.rotation);
                Instantiate(mediumSphere, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            
            if (gameObject.name.Equals("MediumSphere(Clone)"))
            {
                for (int i = 0; i < 4; i++)
                {
                    Instantiate(smallSphere, transform.position, transform.rotation);
                }
                Destroy(gameObject);
            }
        }
    }
}
