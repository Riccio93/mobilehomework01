using UnityEngine;
using UnityEngine.UI;

public class SpawnerBehaviour : MonoBehaviour
{
    public float baseSpawnInterval = 1.0f;
    private float spawnInterval;
    private float nextSpawnTime;
    public GameObject projectile;

    public Slider slider;
    public bool spawnerActive;

    public void Start()
    {
        nextSpawnTime = baseSpawnInterval;
        spawnInterval = baseSpawnInterval;
        spawnerActive = true;
        slider.GetComponentInChildren<Text>().text = "Speed: "+(baseSpawnInterval * slider.value).ToString();
    }

    public void Update()
    {
        if (Time.time > nextSpawnTime)
        {
            nextSpawnTime += spawnInterval;
            if (spawnerActive)
            {
                Instantiate(projectile, transform.position, transform.rotation);
            }
        }
    }

    public void ChangeState()
    {
        spawnerActive = true ? spawnerActive == false : spawnerActive == true;
    }

    public void SliderInterval()
    {
        spawnInterval = baseSpawnInterval / slider.value;
        slider.GetComponentInChildren<Text>().text = "Speed: "+(baseSpawnInterval * slider.value).ToString();
    }
}
